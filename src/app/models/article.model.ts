
export const enum Category {
    EDUCATION = 'EDUCATION',
    GASTRONOMY = 'GASTRONOMY',
    OTHERS = 'OTHERS',
}


export interface Article {
    id?: number;
    title?: string;
    content?: string;
    author?: number;
    category?: Category;
}
