export interface CartItem {
    id?: number;
    ingredientId?: number;
    cartId?: number;
    subTotal?: number;
    quantity?: number;
}

