export interface Cart {
    id?: number;
    username?: string;
    storeId?: number;
    clientId?: number;
    total?: number;
    tva?: number;
}
