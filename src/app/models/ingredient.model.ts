export interface Ingredient {
    id?: number;
    reference?: string;
    name?: string;
    store?: number;
    quantity?: number;
    price?: number;
}